| ID | NAME         | FUNCTION       | NOTES                           |
|----|--------------|----------------|---------------------------------|
|  1 | set Rx D     | Rx <- D        |                                 |
|  2 | mov Rx Ry    | Rx <- Ry       |                                 |
|  3 | add Rx Ry    | Rx <- Rx + Ry  |                                 |
|  4 | sub Rx Ry    | Rx <- Rx - Ry  |                                 |
|  5 | mul Rx Ry    | Rx <- Rx * Ry  |                                 |
|  6 | inv Rx       | Rx <- ~Rx      |                                 |
|  7 | xor Rx Ry    | Rx <- Rx ^ Ry  |                                 |
|  8 | and Rx Ry    | Rx <- Rx & Ry  |                                 |
|  9 | or Rx Ry     | Rx <- Rx | Ry  |                                 |
| 10 | lshift Rx Ry | Rx <- Rx << Ry |                                 |
| 11 | rshift Rx Ry | Rx <- Rx >> Ry |                                 |
| 12 | branch Rx Ry | Pc <- Rx?Ry:PC | goto Ry if Rx != 0              |
| 13 | cmp Rx Op    | Rx <- Rx Op 0  | Op; 0: >0, 1: <0, 2: =0, 3: <>0 |
| 14 | load Rx Ry   | Rx <- RAM[Ry]  |                                 |
| 15 | save Rx Ry   | RAM[Rx] <- Ry  |                                 |

4 Registers + PC (3 bits required)
15 Instructions (5 bits required)
16 bits of data
Align to 8 bits to make it easy to read and easy to implement:
32 Bits per instruction.

Data Alignment
8 Bits instruction
8 Bits register
16 Bits big endian data
Total: 32 bits, 4 bytes

NOTES: PC does not increment when the first register is PC
