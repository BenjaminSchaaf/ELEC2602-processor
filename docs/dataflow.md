# Dataflow

## Description

When loading, we simply read and store 16 bits at the PC from RAM (rising edge).
When executing we load the remaining 16 instruction bits at PC + 1 from RAM on the rising edge, then on the falling edge we increment PC, apply any register changes and move to the next state.
When loading RAM we read 16 bits from RAM on the rising edge and write them to the register on the falling edge.
When saving RAM we simply write 16 bits from the registers to RAM on the rising edge.
When in the fail state, just stay in the fail state.
