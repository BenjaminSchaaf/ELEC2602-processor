LIBRARY ieee;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_signed.ALL;
USE ieee.std_logic_unsigned.ALL;
LIBRARY work;
USE work.types.ALL;

ENTITY arithmetic_logic_unit IS
    PORT (
        operation:     IN alu_op;
        PC:            IN word;
        registerValue: IN word;
        otherValue:    IN word;
        result:        OUT word
    );
END;

ARCHITECTURE behavior OF arithmetic_logic_unit IS
BEGIN
    PROCESS(operation, PC, registerValue, otherValue)
        VARIABLE registerSigned: sword;
        VARIABLE otherSigned:    sword;
        VARIABLE otherUnsigned:  uword;
        VARIABLE otherInteger:   integer;
        VARIABLE tmp32:          dword;

    BEGIN
        registerSigned := signed(registerValue);
        otherSigned    := signed(otherValue);
        otherUnsigned  := unsigned(otherValue);
        otherInteger   := to_integer(otherSigned);

        CASE operation IS
        WHEN assign =>
            result <= otherValue;

        WHEN add =>
            result <= std_logic_vector(registerSigned + otherSigned);

        WHEN sub =>
            result <= std_logic_vector(registerSigned - otherSigned);

        WHEN mul =>
            tmp32 := std_logic_vector(registerSigned * otherSigned);
            result <= tmp32(result'RANGE);

        WHEN inv =>
            result <= NOT registerValue;

        WHEN xor_op =>
            result <= registerValue XOR otherValue;

        WHEN and_op =>
            result <= registerValue AND otherValue;

        WHEN or_op =>
            result <= registerValue OR otherValue;

        WHEN lshift =>
            result <= std_logic_vector(registerSigned SLL otherInteger);

        WHEN rshift =>
            result <= std_logic_vector(registerSigned SRL otherInteger);

        WHEN cmp =>
            IF (otherUnsigned = 0 AND registerSigned > 0) OR
               (otherUnsigned = 1 AND registerSigned < 0) OR
               (otherUnsigned = 2 AND registerSigned = 0) OR
               (otherUnsigned = 3 AND registerSigned /= 0) THEN
                result <= ONE_WORD;
            ELSE
                result <= ZERO_WORD;
            END IF;

        WHEN branch =>
            -- Jump if we don't have 0
            IF registerSigned = 0 THEN
                -- Need to increment manually here
                result <= std_logic_vector(unsigned(PC) + 2);
            ELSE
                result <= otherValue;
            END IF;

        END CASE;
    END PROCESS;
END;
