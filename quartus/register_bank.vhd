LIBRARY ieee;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
LIBRARY work;
USE work.types.ALL;

ENTITY register_bank IS
    PORT (
        clk, incrementPC: IN std_logic;
        writeEnable:      IN std_logic;
        writeValue:       IN word;
        writeRegister:    IN byte;
        readValues:       OUT register_array
    );
END;

ARCHITECTURE behavior OF register_bank IS
    SIGNAL registers: register_array := (others => (others => '0'));

BEGIN
    PROCESS(clk)
    BEGIN
        IF falling_edge(clk) THEN
            -- Special case for PC,
            -- so we can write to a register and increment PC in the same cycle
            IF incrementPC = '1' THEN
                registers(0) <= registers(0) + 2;
            END IF;

            IF writeEnable = '1' THEN
                registers(to_integer(unsigned(writeRegister))) <= writeValue;
            END IF;
        END IF;
    END PROCESS;

    -- Always read all registers
    readValues <= registers;
END;
