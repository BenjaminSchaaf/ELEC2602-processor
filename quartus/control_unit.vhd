LIBRARY ieee;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
LIBRARY work;
USE work.types.ALL;

ENTITY control_unit IS
    PORT (
        instructionData:    IN dword;
        state:              IN dawson_state;
        hasError:           IN std_logic;
        nextState:          OUT dawson_state;
        aluOperation:       OUT alu_op;
        aluValue:           OUT word;
        aluValueIsRegister: OUT std_logic;
        aluRegister:        OUT byte;
        writeRegister:      OUT byte;
        rbWriteEnable:      OUT std_logic;
        mcWriteEnable:      OUT std_logic;
        incrementPC:        OUT std_logic
    );
END;

ARCHITECTURE behavior OF control_unit IS
    COMPONENT instruction_decoder IS
        PORT (
            instruction: IN dword;
            id:          OUT instruction_id;
            reg:         OUT byte;
            data:        OUT word
        );
    END COMPONENT;

    SIGNAL id, idOut:   instruction_id;
    SIGNAL reg, regOut: byte;
    SIGNAL data:        word;

    SIGNAL nextWriteRegister: byte;
    SIGNAL nextIncrementPC:   std_logic;

BEGIN
    -- when our state is exec, we want to write to the registers
    rbWriteEnable <= '1' WHEN state = exec OR state = loadRAM ELSE '0';

    -- when our state is save Save RAM, we want to write to RAM
    mcWriteEnable <= '1' WHEN state = saveRAM ELSE '0';

    decoder: instruction_decoder PORT MAP (
        instruction => instructionData,
        id          => idOut,
        reg         => regOut,
        data        => data
    );

    -- Don't need to process the data, its always valid
    aluValue <= data;

    -- Handle invalid instructions
    -- This process may set id to invalid
    PROCESS(idOut, reg, regOut, data, hasError)
        VARIABLE nextId: instruction_id;
        VARIABLE nextValueIsRegister: std_logic;
    BEGIN
        nextId := idOut;

        -- If there's already an error, make the instruction erronous
        IF hasError = '1' THEN
            nextId := invalid;
        END IF;

        -- Handle invalid registers as invalid instruction
        -- The register count excludes PC (register 0)
        IF unsigned(regOut) > REGISTER_COUNT THEN
            nextId := invalid;
            reg <= ZERO_BYTE;
        ELSE
            reg <= regOut;
        END IF;

        -- Decide whether the value should be a register
        IF nextId = invalid OR nextId = set OR nextId = cmp THEN
            nextValueIsRegister := '0';
        ELSE
            nextValueIsRegister := '1';
        END IF;

        -- Handle invalid value when the value is a register as invalid instruction
        IF nextValueIsRegister = '1' AND unsigned(data) >= REGISTER_COUNT THEN
            id <= invalid;
            nextValueIsRegister := '0'; -- Need to set this or simulations fail
        END IF;

        id <= nextId;
        aluValueIsRegister <= nextValueIsRegister;
    END PROCESS;

    aluRegister <= reg;

    -- branch instruction writes
    writeRegister <= ZERO_BYTE WHEN id = branch ELSE reg;

    -- Handle incrementPC
    PROCESS(id, reg, state)
    BEGIN
        CASE state IS
        WHEN exec =>
        -- Only branch doesn't increment PC or when reg is the PC
            IF unsigned(reg) = 0 OR id = branch THEN
                incrementPC <= '0';
            ELSE
                incrementPC <= '1';
            END IF;

        WHEN load =>
            incrementPC <= '0';

        WHEN OTHERS =>
            incrementPC <= '0';
        END CASE;
    END PROCESS;

    -- Handle nextState transitions
    PROCESS(id, state)
    BEGIN
        CASE state IS
        WHEN exec =>
            CASE id IS
            WHEN invalid =>  nextState <= fail;
            WHEN load =>     nextState <= loadRAM;
            WHEN save =>     nextState <= saveRAM;
            WHEN OTHERS =>   nextState <= load;
            END CASE;

        WHEN load =>     nextState <= exec;
        WHEN loadRAM =>  nextState <= load;
        WHEN saveRAM =>  nextState <= load;
        WHEN fail =>     nextState <= fail;
        END CASE;
    END PROCESS;

    -- Handle ALU operation
    PROCESS(id)
    BEGIN
        CASE id IS
        WHEN add =>     aluOperation <= add;
        WHEN sub =>     aluOperation <= sub;
        WHEN mul =>     aluOperation <= mul;
        WHEN inv =>     aluOperation <= inv;
        WHEN xor_op =>  aluOperation <= xor_op;
        WHEN and_op =>  aluOperation <= and_op;
        WHEN or_op =>   aluOperation <= or_op;
        WHEN lshift =>  aluOperation <= lshift;
        WHEN rshift =>  aluOperation <= rshift;
        WHEN branch =>  aluOperation <= branch;
        WHEN cmp =>     aluOperation <= cmp;
        WHEN OTHERS =>  aluOperation <= assign;
        END CASE;
    END PROCESS;
END;
