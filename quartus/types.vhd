LIBRARY ieee;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;

-- Package for defining some common types and constants
PACKAGE types IS
    SUBTYPE byte  IS std_logic_vector(7 DOWNTO 0);
    SUBTYPE ubyte IS unsigned(byte'RANGE);
    SUBTYPE word  IS std_logic_vector(15 DOWNTO 0);
    SUBTYPE uword IS unsigned(word'RANGE);
    SUBTYPE sword IS signed(word'RANGE);
    SUBTYPE dword IS std_logic_vector(31 DOWNTO 0);

    CONSTANT REGISTER_COUNT: integer := 4;

    CONSTANT ZERO_BYTE: byte := (OTHERS => '0');
    CONSTANT ZERO_WORD: word := (OTHERS => '0');
    CONSTANT ONE_WORD:  word := "0000000000000001";

    -- Use an extra register for PC (R0)
    TYPE register_array IS array(0 TO REGISTER_COUNT) of word;

    TYPE dawson_state IS (load, exec, loadRAM, saveRAM, fail);

    TYPE alu_op IS (assign, add, sub, mul, inv, xor_op, and_op, or_op, lshift, rshift, cmp, branch);

    TYPE instruction_id IS (invalid, set, mov, add, sub, mul, inv, xor_op, and_op, or_op, lshift, rshift, branch, cmp, load, save);
END;

PACKAGE BODY types IS
END;
