LIBRARY ieee;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
LIBRARY work;
USE work.types.ALL;

ENTITY dawson IS
    PORT (
        clk:                IN std_logic;
        registerInspection: OUT register_array;
        readState:          OUT dawson_state
    );
END;

ARCHITECTURE behavior OF dawson IS
    -- RB
    SIGNAL rbWriteEnable: std_logic;
    SIGNAL rbWriteValue:  word;

    COMPONENT register_bank IS
        PORT (
            clk, incrementPC: IN std_logic;
            writeEnable:      IN std_logic;
            writeValue:       IN word;
            writeRegister:    IN byte;
            readValues:       OUT register_array
        );
    END COMPONENT;

    SIGNAL rbReadValues: register_array;

    -- Shortcut to the PC
    ALIAS PC: word IS rbReadValues(0);

    -- RAM
    SIGNAL address:       word      := ZERO_WORD;
    SIGNAL mcWriteEnable: std_logic := '0';
    SIGNAL mcHasError:    std_logic := '0';

    COMPONENT memory_controller IS
        PORT (
            clk:         IN std_logic;
            writeEnable: IN std_logic;
            address:     IN word;
            writeWord:   IN word;
            readWord:    OUT word;
            hasError:    OUT std_logic
        );
    END COMPONENT;

    SIGNAL mcReadWord: word;

    -- CU
    SIGNAL instructionData: dword;
    SIGNAL writeRegister:   byte;

    COMPONENT control_unit IS
        PORT (
            instructionData:    IN dword;
            state:              IN dawson_state;
            hasError:           IN std_logic;
            nextState:          OUT dawson_state;
            aluOperation:       OUT alu_op;
            aluValue:           OUT word;
            aluValueIsRegister: OUT std_logic;
            aluRegister:        OUT byte;
            writeRegister:      OUT byte;
            rbWriteEnable:      OUT std_logic;
            mcWriteEnable:      OUT std_logic;
            incrementPC:        OUT std_logic
        );
    END COMPONENT;

    SIGNAL nextState:          dawson_state;
    SIGNAL aluOperation:       alu_op;
    SIGNAL aluValue:           word;
    SIGNAL aluValueIsRegister: std_logic;
    SIGNAL aluRegister:        byte;
    SIGNAL incrementPC:        std_logic;

    -- ALU
    SIGNAL aluRegisterValue: word;
    SIGNAL aluOtherValue:    word;

    COMPONENT arithmetic_logic_unit IS
        PORT (
            operation:     IN alu_op;
            PC:            IN word;
            registerValue: IN word;
            otherValue:    IN word;
            result:        OUT word
        );
    END COMPONENT;

    SIGNAL aluResult: word;

    -- State
    SIGNAL state:               dawson_state := load;
    SIGNAL stateTopInstruction: word         := ZERO_WORD;

BEGIN
    -- MUX1
    rbWriteValue <= aluResult WHEN state = exec ELSE mcReadWord;

    rb: register_bank PORT MAP (
        clk           => clk,
        incrementPC   => incrementPC,
        writeEnable   => rbWriteEnable,
        writeValue    => rbWriteValue,
        writeRegister => writeRegister,
        readValues    => rbReadValues
    );

    address <= PC               WHEN state = load
          ELSE PC + 1           WHEN state = exec
          ELSE aluResult        WHEN state = loadRAM
          ELSE aluRegisterValue WHEN state = saveRAM
          ELSE (OTHERS => '0');

    mc: memory_controller PORT MAP (
        clk         => clk,
        writeEnable => mcWriteEnable,
        address     => address,
        writeWord   => aluResult,
        readWord    => mcReadWord,
        hasError    => mcHasError
    );

    -- COMB
    instructionData(31 DOWNTO 16) <= stateTopInstruction;
    instructionData(15 DOWNTO 0)  <= mcReadWord;

    cu: control_unit PORT MAP (
        instructionData    => instructionData,
        state              => state,
        hasError           => mcHasError,
        nextState          => nextState,
        aluOperation       => aluOperation,
        aluValue           => aluValue,
        aluValueIsRegister => aluValueIsRegister,
        aluRegister        => aluRegister,
        writeRegister      => writeRegister,
        rbWriteEnable      => rbWriteEnable,
        mcWriteEnable      => mcWriteEnable,
        incrementPC        => incrementPC
    );

    -- MUX2
    -- Mux for deciding whether to use the value from the cu or read the register at the value
    aluOtherValue <= aluValue WHEN aluValueIsRegister = '0'
                ELSE rbReadValues(to_integer(unsigned(aluValue))); -- MUX3

    -- MUX4
    aluRegisterValue <= rbReadValues(to_integer(unsigned(aluRegister)));

    alu: arithmetic_logic_unit PORT MAP (
        operation     => aluOperation,
        PC            => PC,
        registerValue => aluRegisterValue,
        otherValue    => aluOtherValue,
        result        => aluResult
    );

    PROCESS(clk, state, nextState, mcReadWord)
    BEGIN
        IF falling_edge(clk) THEN
            IF state = load THEN
                stateTopInstruction <= mcReadWord;
            END IF;

            state <= nextState;
        END IF;
    END PROCESS;

    -- Outputs
    registerInspection <= rbReadValues;
    readState          <= state;
END;
