LIBRARY ieee;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
LIBRARY work;
USE work.types.ALL;

ENTITY instance IS
    PORT (
        SW:                     IN byte;
        KEY:                    IN std_logic_vector(0 DOWNTO 0);
        LEDR:                   OUT std_logic_vector(2 DOWNTO 0);
        HEX0, HEX1, HEX2, HEX3: OUT std_logic_vector(6 DOWNTO 0)
    );
END;

ARCHITECTURE behavior OF instance IS
    COMPONENT dawson IS
        PORT (
            clk:                IN std_logic;
            registerInspection: OUT register_array;
            readState:          OUT dawson_state
        );
    END COMPONENT;

    COMPONENT sevensegment_encoder IS
        PORT (
            value:    IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            sevenSeg: OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
        );
    END COMPONENT;

    SIGNAL registerInspection: register_array;
    SIGNAL registerValue:      word;
    SIGNAL readState:          dawson_state;
BEGIN
    cpu: dawson PORT MAP (
        clk                => NOT KEY(0),
        registerInspection => registerInspection,
        readState          => readState
    );

    registerValue <= registerInspection(to_integer(unsigned(SW)));

    hex_encoder0: sevensegment_encoder PORT MAP (
        value    => registerValue(3 DOWNTO 0),
        sevenSeg => HEX0
    );

    hex_encoder1: sevensegment_encoder PORT MAP (
        value    => registerValue(7 DOWNTO 4),
        sevenSeg => HEX1
    );

    hex_encoder2: sevensegment_encoder PORT MAP (
        value    => registerValue(11 DOWNTO 8),
        sevenSeg => HEX2
    );

    hex_encoder3: sevensegment_encoder PORT MAP (
        value    => registerValue(15 DOWNTO 12),
        sevenSeg => HEX3
    );

    LEDR <= std_logic_vector(to_unsigned(dawson_state'POS(readState), LEDR'LENGTH));
END;
