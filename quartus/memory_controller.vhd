LIBRARY ieee;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
LIBRARY work;
USE work.types.ALL;
USE work.ram_config.ALL;

ENTITY memory_controller IS
    PORT (
        clk:         IN std_logic;
        writeEnable: IN std_logic;
        address:     IN word;
        writeWord:   IN word;
        readWord:    OUT word;
        hasError:    OUT std_logic
    );
END;

ARCHITECTURE behavioral OF memory_controller IS
    COMPONENT random_access_memory IS
        PORT (
            clk, writeEnable: IN std_logic;
            address:          IN integer;
            writeValue:       IN word;
            readValue:        OUT word
        );
    END COMPONENT;

    SIGNAL addressInteger: integer := 0;

BEGIN
    PROCESS(address)
        VARIABLE index: integer;
    BEGIN
        index := to_integer(unsigned(address));

        IF index >= MEMORY_LIMIT THEN
            addressInteger <= 0;
            hasError <= '1';
        ELSE
            addressInteger <= index;
            hasError <= '0';
        END IF;
    END PROCESS;

    ram: random_access_memory PORT MAP (
        clk         => clk,
        writeEnable => writeEnable,
        address     => addressInteger,
        writeValue  => writeWord,
        readValue   => readWord
    );
END;
