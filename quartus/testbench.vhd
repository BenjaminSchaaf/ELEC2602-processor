LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
LIBRARY std;
use std.textio.ALL;
LIBRARY work;
USE work.types.ALL;

ENTITY testbench IS
END testbench;

ARCHITECTURE behavior OF testbench IS
    -- Components
    COMPONENT dawson IS
        PORT (
            clk:                IN std_logic;
            registerInspection: OUT register_array;
            readState:          OUT dawson_state
        );
    END COMPONENT;

    SIGNAL registerInspection: register_array;
    SIGNAL readState:          dawson_state;

   -- State
    SIGNAL clk:   std_logic := '0';
    SIGNAL count: integer   := 0;

BEGIN
    cpu: dawson PORT MAP (
        clk                => clk,
        registerInspection => registerInspection,
        readState          => readState
    );

    PROCESS
    BEGIN
        -- 50Mhz clock
        WAIT FOR 20 ns;

        clk <= NOT clk;
    END PROCESS;

    PROCESS(clk, count)
    BEGIN
        IF rising_edge(clk) THEN
            count <= count + 1;
        END IF;
    END PROCESS;
END;
