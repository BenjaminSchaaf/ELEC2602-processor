LIBRARY ieee;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
LIBRARY work;
USE work.types.ALL;

ENTITY instruction_decoder IS
    PORT (
        instruction: IN dword;
        id:          OUT instruction_id;
        reg:         OUT byte;
        data:        OUT word
    );
END;

ARCHITECTURE behavior OF instruction_decoder IS
    -- Get the unsigned value of the highest instruction
    CONSTANT HIGHEST_INSTRUCTION: ubyte := to_unsigned(instruction_id'POS(instruction_id'HIGH), ubyte'LENGTH);

BEGIN
    data <= instruction(15 DOWNTO 0);
    reg  <= instruction(23 DOWNTO 16);

    PROCESS(instruction)
        VARIABLE uid: ubyte;
    BEGIN
        uid := unsigned(instruction(31 DOWNTO 24));

        -- Handle instructions greater than the highest instruction value: normalize them to `invalid`
        IF uid <= HIGHEST_INSTRUCTION THEN
            id <= instruction_id'VAL(to_integer(uid));
        ELSE
            id <= invalid;
        END IF;
    END PROCESS;
END;
