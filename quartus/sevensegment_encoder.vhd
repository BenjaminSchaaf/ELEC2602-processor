LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY sevensegment_encoder IS
    PORT (
        value:    IN std_logic_vector(3 DOWNTO 0);
        sevenSeg: OUT std_logic_vector(6 DOWNTO 0)
    );
END;

ARCHITECTURE behavior OF sevensegment_encoder IS
BEGIN
    PROCESS(value)
    BEGIN
        CASE value IS
        WHEN "0000" =>  sevenSeg <= "1000000"; -- 0
        WHEN "0001" =>  sevenSeg <= "1111001"; -- 1
        WHEN "0010" =>  sevenSeg <= "0100100"; -- 2
        WHEN "0011" =>  sevenSeg <= "0110000"; -- 3
        WHEN "0100" =>  sevenSeg <= "0011001"; -- 4
        WHEN "0101" =>  sevenSeg <= "0010010"; -- 5
        WHEN "0110" =>  sevenSeg <= "0000010"; -- 6
        WHEN "0111" =>  sevenSeg <= "1111000"; -- 7
        WHEN "1000" =>  sevenSeg <= "0000000"; -- 8
        WHEN "1001" =>  sevenSeg <= "0010000"; -- 9
        WHEN "1010" =>  sevenSeg <= "0001000"; -- A
        WHEN "1011" =>  sevenSeg <= "0000011"; -- b
        WHEN "1100" =>  sevenSeg <= "1000110"; -- C
        WHEN "1101" =>  sevenSeg <= "0100001"; -- d
        WHEN "1110" =>  sevenSeg <= "0000110"; -- E
        WHEN "1111" =>  sevenSeg <= "0001110"; -- F
        END CASE;
    END PROCESS;
END;
