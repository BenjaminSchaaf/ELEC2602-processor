LIBRARY ieee;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
LIBRARY work;
USE work.types.ALL;
USE work.ram_config.ALL;

-- 16 bit memory module, hopefully quartus infers this as RAM instead of registers
ENTITY random_access_memory IS
    PORT (
        clk, writeEnable: IN std_logic;
        address:          IN integer;
        writeValue:       IN word;
        readValue:        OUT word
    );
END;

ARCHITECTURE behavioral OF random_access_memory IS
    SIGNAL RAM: memory_array := init_ram;

BEGIN
    PROCESS(clk, writeEnable)
    BEGIN
        IF rising_edge(clk) THEN
            IF writeEnable = '1' THEN
                RAM(address) <= writeValue;
            END IF;

            readValue <= RAM(address);
        END IF;
    END PROCESS;
END;
