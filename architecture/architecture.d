module architecture;

import std.conv;
import std.stdio;
import std.string;
import std.digest.digest;
import std.bitmanip;
import std.exception;
import std.algorithm;

const REGISTER_COUNT = 4;
static assert(REGISTER_COUNT <= 16);

const MEMORY_SIZE = ushort.max;
static assert(MEMORY_SIZE <= ushort.max);

struct Instruction {
    enum ID : ubyte {
        set = 1,
        mov = 2,
        add = 3,
        sub = 4,
        mul = 5,
        inv = 6,
        xor = 7,
        and = 8,
        or = 9,
        lshift = 10,
        rshift = 11,
        branch = 12,
        cmp = 13,
        load = 14,
        save = 15,
    }

    ID id;
    ubyte register;
    ushort data;

    static Instruction decode(ubyte[4] input) {
        const id = cast(ID)input[0];
        const register = cast(ubyte)input[1];
        const data = input[2..4].bigEndianToNative!ushort;

        return Instruction(id, register, data);
    }

    ubyte[4] encode() {
        ubyte[4] result;
        result[0] = id;
        result[1] = register;
        result[2..4] = nativeToBigEndian(data);
        return result;
    }

    unittest {
        assert(Instruction(ID.set, 0, 0).encode() == [0x01, 0x00, 0, 0]);
        assert(Instruction(ID.set, 1, 0).encode() == [0x01, 0x01, 0, 0]);
        assert(Instruction(ID.mov, 0, 0).encode() == [0x02, 0x00, 0, 0]);
        assert(Instruction(ID.mov, 1, 200).encode() == [0x02, 0x01, 0, 200]);
        assert(Instruction(ID.lshift, 2, 32767).encode() == [0x0A, 0x02, 127, 255]);
    }

    string toString() {
        if (id == ID.set) {
            return "%s %s %s".format(id.to!string, regString(register), nativeToBigEndian(data).toHexString());
        } else if ([Instruction.ID.inv].canFind(id)) {
            return "%s %s".format(id.to!string, regString(register));
        } else if (id == Instruction.ID.cmp) {
            return "%s %s %s".format(id.to!string, regString(register), [">0", "<0", "=0", "<>0"][data]);
        } else {
            return "%s %s %s".format(id.to!string, regString(register), regString(cast(ubyte)data));
        }
    }

    private @property string regString(ubyte register) {
        if (register == 0) {
            return "PC";
        }
        return "R%s".format(register);
    }
}
