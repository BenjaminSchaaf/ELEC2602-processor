# Architecture Tools

## Assembly Example

```
    set R0 12
    set R1 :somecodelabel

somecodelabel:
    set R2 -4
    set R3 0xffff
; comment
some-other-data-label: 123
some_label: 0xfaebfebafbabfbebebefae98198239845925
```

Turns into:

```
00000000  10 00 0c 11 00 06 12 ff  fc 13 ff ff 00 7b fa eb  |.............{..|
00000010  fe ba fb ab fb eb eb ef  ae 98 19 82 39 84 59 25  |............9.Y%|
00000020
```
