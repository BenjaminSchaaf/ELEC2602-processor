import core.thread;

import std.stdio;
import std.range;
import std.string;
import std.digest.digest;
import std.bitmanip;
import std.algorithm;
import std.exception;

import architecture;

void main(string[] args) {
    auto ram = stdin.byChunk(4096).joiner.array;

    auto vm = ProcVM!4(ram);

    scope(exit) {
        writeln();
        writeln(vm.programPointer, " ", vm.registers);

        writeln("0x", vm.ram.toHexString());
    }

    while (true) {
        auto instruction = vm.step();

        auto count = 0;
        if (args.canFind("--instr")) {
            writeln(instruction.toString());
            count += 1;
        }

        if (args.canFind("--pc")) {
            writeln(vm.programPointer, " ", vm.registers);
            count += 1;
        }

        if (args.canFind("--ram")) {
            writeln("0x", vm.ram.toHexString());
            count += 1;
        }

        if (count > 1) {
            writeln();
        }

        if (args.canFind("--slow")) {
            Thread.sleep(500.msecs);
        }
    }
}

struct ProcVM(ubyte registerCount) {
    static assert(registerCount <= 16);

    // An extra register for PC
    ushort[registerCount + 1] registers;
    ubyte[] ram;

    this(ubyte[] ram) {
        assert(ram.length <= ushort.max);

        this.ram = ram;
    }

    @property ref ushort programPointer() { return registers[0]; }

    Instruction step() {
        const ubyte[4] instructionData = ram[programPointer * 2..$][0..4];
        const instruction = Instruction.decode(instructionData);

        switch (instruction.id) {
            case Instruction.ID.set:
                registers[instruction.register] = instruction.data;
                break;

            case Instruction.ID.mov:
                registers[instruction.register] = registers[instruction.data];
                break;

            case Instruction.ID.add:
                registers[instruction.register] += registers[instruction.data];
                break;

            case Instruction.ID.sub:
                registers[instruction.register] -= registers[instruction.data];
                break;

            case Instruction.ID.mul:
                registers[instruction.register] *= registers[instruction.data];
                break;

            case Instruction.ID.inv:
                registers[instruction.register] = ~registers[instruction.register];
                break;

            case Instruction.ID.xor:
                registers[instruction.register] ^= registers[instruction.data];
                break;

            case Instruction.ID.and:
                registers[instruction.register] &= registers[instruction.data];
                break;

            case Instruction.ID.or:
                registers[instruction.register] |= registers[instruction.data];
                break;


            case Instruction.ID.lshift:
                registers[instruction.register] = cast(ushort)(registers[instruction.register] << registers[instruction.data]);
                break;

            case Instruction.ID.rshift:
                registers[instruction.register] = registers[instruction.register] >> registers[instruction.data];
                break;

            case Instruction.ID.branch:
                if (registers[instruction.register]) {
                    programPointer = registers[instruction.data];
                    return instruction;
                }
                break;

            case Instruction.ID.cmp:
                const cmp = instruction.data;
                assert(cmp < 4, "Invalid comparator");

                const value = registers[instruction.register];
                registers[instruction.register] =
                    cmp == 0 && cast(short)value > 0 ||
                    cmp == 1 && cast(short)value < 0 ||
                    cmp == 2 && value == 0 ||
                    cmp == 3 && value != 0;
                break;

            case Instruction.ID.load:
                const index = registers[instruction.data] * 2;
                const ubyte[2] memory = ram[index..$][0..2];
                const value = bigEndianToNative!ushort(memory);

                registers[instruction.register] = value;
                break;

            case Instruction.ID.save:
                const value = nativeToBigEndian(registers[instruction.data]);
                const index = registers[instruction.register] * 2;
                ram[index..$][0..2] = value;
                break;

            default:
                assert(0, "Invalid Instruction %s".format(cast(ubyte)instruction.id));
        }

        if (instruction.register != 0) {
            programPointer += 2;
        }

        return instruction;
    }
}
