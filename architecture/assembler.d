import std.conv;
import std.stdio;
import std.range;
import std.array;
import std.string;
import std.typecons;
import std.bitmanip;
import std.algorithm;
import std.exception;

import architecture;

void main(string[] args) {
    Parser p;

    p.parse(stdin.byLine());

    if (args.canFind("--labels")) stderr.writeln(p.labels);

    stdout.rawWrite(p.data);
}

struct Parser {
    ubyte[] data;
    ushort[string] labels;
    Tuple!(string, ushort)[] replacementList;

    void parse(R)(R input) if (isInputRange!R) {
        foreach (line; input) {
            try {
                parseLine(line.idup);
            } catch (Exception e) {
                throw new Exception("Error in line: %s".format(line), e);
            }
        }

        replaceLabels();
    }

    private void parseLine(string line) {
        if (line.startsWith(";") || line.strip() == "") {
            return;
        }

        if (line.startsWith("    ")) {
            auto parts = line[4..$].split(" ").map!strip.array();

            enforce(parts.length > 1 && parts.length <= 3, "Wrong number of parts for instruction: " ~ line);

            auto instruction = parts[0].to!(Instruction.ID);

            auto register = parseRegister(parts[1]);

            if (instruction == Instruction.ID.set) {
                enforce(parts.length == 3);

                auto dataStr = parts[2];
                ushort value;

                // TODO: Handle labels
                if (dataStr.startsWith(":")) {
                    replacementList ~= tuple(dataStr[1..$], cast(ushort)(data.length + 2));
                } else {
                    auto parsed = parseData(dataStr);
                    enforce(parsed.length == 2);
                    value = parsed[0..2].bigEndianToNative!ushort;
                }

                data ~= Instruction(instruction, register, value).encode();
            } else if (instruction == Instruction.ID.cmp) {
                enforce(parts.length == 3);

                const comparator = cast(ushort)[">0", "<0", "=0", "<>0"].countUntil(parts[2]);

                data ~= Instruction(instruction, register, comparator).encode();
            } else if ([Instruction.ID.inv].canFind(instruction)) {
                enforce(parts.length == 2);

                data ~= Instruction(instruction, register).encode();
            } else {
                enforce(parts.length == 3);

                auto otherRegister = parseRegister(parts[2]);

                data ~= Instruction(instruction, register, otherRegister).encode();
            }
        } else {
            auto parts = line.split(":").map!strip.array();
            assert(parts.length == 2);

            auto name = parts[0];
            auto value = parts[1];

            // Data is indexed by 16 bits
            labels[name] = cast(ushort)data.length / 2;

            if (value != "") {
                const labelData = parseData(parts[1]);
                assert(labelData.length % 2 == 0);
                data ~= labelData;
            }
        }
    }

    private void replaceLabels() {
        //stderr.writeln(labels);

        foreach (replacement; replacementList) {
            auto label = replacement[0];
            auto location = replacement[1];

            enforce(label in labels);

            data[location..location + 2] = nativeToBigEndian(labels[label]);
        }
    }
}

ubyte parseRegister(string str) {
    if (str.startsWith("R")) {
        return cast(ubyte)(str[1..$].to!ubyte + 1);
    }
    enforce(str == "PC");

    return 0;
}

unittest {
    foreach (i; 0..200) {
        assert(parseRegister("R" ~ i.to!string) == i + 1);
    }

    try {
        parseRegister("");
        assert(0);
    } catch (Exception e) {}

    try {
        parseRegister("R-50");
        assert(0);
    } catch (Exception e) {}

    try {
        parseRegister("RA");
        assert(0);
    } catch (Exception e) {}
}

ubyte[] parseData(string str) {
    if (str.startsWith("0x")) {
        return str[2..$].chunks(2).map!(d => d.parse!ubyte(16)).array();
    } if (str.startsWith("0b")) {
        return str[2..$].chunks(8).map!(d => d.parse!ubyte(2)).array();
    } else {
        return nativeToBigEndian(str.to!short).dup;
    }
}

unittest {
    assert(parseData("0x") == []);
    assert(parseData("0xFF") == [255]);

    assert(parseData("0b") == []);
    assert(parseData("0b00000100") == [4]);

    assert(parseData("0") == [0, 0]);
    assert(parseData("1") == [0, 1]);
    assert(parseData("255") == [0, 255]);
    assert(parseData("256") == [1, 0]);
    assert(parseData("32767") == [127, 255]);
    assert(parseData("-32768") == [128, 0]);
    assert(parseData("-1") == [255, 255]);
}
