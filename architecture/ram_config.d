import std.stdio;
import std.range;
import std.array;
import std.format;
import std.algorithm;

const output = `LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
LIBRARY work;
USE work.types.ALL;

PACKAGE ram_config IS
    CONSTANT MEMORY_LIMIT: integer := 640000;

    TYPE memory_array IS array(0 TO MEMORY_LIMIT - 1) of word;

    CONSTANT init_ram: memory_array := (%s);
END;

PACKAGE BODY ram_config IS
END;`;

void main() {
    auto byteChunks = stdin.byChunk(4096).joiner.array;

    string constant = "\n        ";
    foreach (chunk; byteChunks.chunks(2)) {
        constant ~= "\"";
        foreach (value; chunk) {
            constant ~= "%08b".format(value);
        }
        foreach (index; chunk.length..2) {
            constant ~= "00000000";
        }

        constant ~= "\",\n        ";
    }
    constant ~= "OTHERS => (OTHERS => '0')\n    ";

    writeln(output.format(constant));
}
